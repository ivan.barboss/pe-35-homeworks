"use strict";

const imgs = document.querySelectorAll(".image-to-show");
const end = document.querySelector(".btn");
const again = document.querySelector(".btn2");
let n = 1;
let interval = 0;
function slide() {
  imgs.forEach((elem) => elem.classList.remove("active"));
  imgs[n].classList.add("active");
  n++;
  if (n === imgs.length) {
    n = 0;
  }
}
function show() {
  if (interval == 0) {
    interval = setInterval(slide, 2000);
  }
}
function stop() {
  clearInterval(interval);
  interval = 0;
}
end.onclick = function () {
  stop();
};
again.onclick = function () {
  show();
};
window.onload = function () {
  show();
};
