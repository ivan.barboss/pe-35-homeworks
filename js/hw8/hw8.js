"use strict";
console.dir(window);

let inputIn = document.querySelector(".input-in");
let error = document.createElement("div");
error.textContent = "Please enter correct price";
let divBeforeSpan = document.createElement("div");
let spanValue = document.createElement("span");
let buttonX = document.createElement("button");
buttonX.textContent = `x`;

inputIn.onblur = function onblur() {
  if (
    !inputIn.value ||
    isNaN(inputIn.value) ||
    inputIn.value.trim() === "" ||
    inputIn.value <= 0 ||
    typeof +inputIn.value !== "number"
  ) {
    inputIn.style.borderColor = "red";
    inputIn.after(error);
  } else {
    inputIn.before(divBeforeSpan);
    divBeforeSpan.append(spanValue);
    spanValue.append(`Текущая цена: ${inputIn.value}`);
    spanValue.append(buttonX);
    inputIn.style.borderColor = "green";
    error.innerHTML = "";
  }
};

const onfocusFun = function () {
  inputIn.style.borderColor = "green";
};

inputIn.addEventListener("focus", onfocusFun);

buttonX.onclick = function () {
  spanValue.innerHTML = "";
  inputIn.value = "";
};
