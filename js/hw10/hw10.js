"use strict";
const form = document.getElementById("pass-form");
const btn = document.getElementsByClassName("btn");
let divBeforeSpan = document.createElement("div");
let spanText = document.createElement("span");

const passwordF = () => {
  let icons = document.getElementsByClassName("fas");
  const passInput = document.getElementById("password-input");
  const passConfirm = document.getElementById("password-input-confirm");
  let atr = passInput.getAttribute("type");
  if (atr === "password") {
    passInput.setAttribute("type", "text");
    passConfirm.setAttribute("type", "text");
    icons[0].className = "fas fa-eye-slash icon-password";
    icons[1].className = "fas fa-eye-slash icon-password";
  } else {
    passInput.setAttribute("type", "password");
    passConfirm.setAttribute("type", "password");
    icons[0].className = "fas fa-eye icon-password";
    icons[1].className = "fas fa-eye icon-password";
  }
};

const submit = (event) => {
  const passInput = document.getElementById("password-input");
  const passConfirm = document.getElementById("password-input-confirm");
  if (passInput.value == passConfirm.value) {
    alert("You are welcom");
    return;
  } else {
    passConfirm.after(divBeforeSpan);
    divBeforeSpan.append(spanText);
    spanText.append("Нужно ввести одинаковые значения");
    spanText.style.color = "red";
    return;
  }
};
form.addEventListener("submit", submit, false);
btn.addEventListener;
