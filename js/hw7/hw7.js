"use strict";
const root = document.getElementById("root");
function createList(arrs, parent = document.body) {
  const list = arrs.map((element) => {
    return `<li>${element}</li>`;
  });
  parent.insertAdjacentHTML("afterbegin", `<ul>${list.join(" ")}</ul>`);
}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
