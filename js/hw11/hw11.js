"use strict";

let btn = Array.from(document.querySelectorAll(".btn"));
function btnDown(letter) {
  let key = btn.filter((item) => item.textContent === letter);
  key.forEach((el) => (el.style.backgroundColor = "blue"));
  let arr = btn.filter((item) => item.textContent !== letter);
  arr.forEach((el) => (el.style.backgroundColor = "black"));
}
document.addEventListener("keydown", (e) => {
  if (e.code === "Enter") {
    btnDown("Enter");
  }
  if (e.code === "KeyS") {
    btnDown("S");
  }
  if (e.code === "KeyE") {
    btnDown("E");
  }
  if (e.code === "KeyO") {
    btnDown("O");
  }
  if (e.code === "KeyN") {
    btnDown("N");
  }
  if (e.code === "KeyL") {
    btnDown("L");
  }
  if (e.code === "KeyZ") {
    btnDown("Z");
  }
});
