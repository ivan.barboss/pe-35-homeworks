"use strict";
function createNewUser() {
  const firstName = prompt("Enter your name");
  const lastName = prompt("Enter your lastname");
  let birthday = prompt("dd.mm.yyyy");
  let newUser = {
    firstName,
    lastName,
    birthday,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge: function () {
      const userBirthdayValue = this.birthday.split(".");
      const userBirthdayDate = new Date(
        userBirthdayValue[2],
        userBirthdayValue[1],
        userBirthdayValue[0]
      );
      const userAgeDifference = Date.now() - userBirthdayDate.getTime();
      const userAge = new Date(userAgeDifference);
      return userAge.getFullYear() - 1970;
    },
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
  newUser.login = newUser.getLogin();
  newUser.age = newUser.getAge();
  newUser.password = newUser.getPassword();
  return newUser;
}

console.log(createNewUser());
