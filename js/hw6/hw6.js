"use strict";
function filterBy(array, type) {
  return array.reduce((res, currentItem) => {
    if (typeof currentItem != type) {
      res.push(currentItem);
    }
    return res;
  }, []);
}
