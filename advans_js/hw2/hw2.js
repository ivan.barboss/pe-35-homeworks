const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

function authors(array, selector) {
  const ul = document.createElement("ul");
  let newArray = array.map((elem) => {
    try {
      if (!elem.author) {
        throw new Error("Автор не найден");
      }
      if (!elem.name) {
        throw new Error("Название не найдено");
      }
      if (!elem.price) {
        throw new Error("Цена не найдена");
      }
      let item = document.createElement("li");
      item.innerHTML = `${elem.author}, ${elem.name}, ${elem.price}`;
      item.className = "item";
      ul.append(item);
      document.querySelector(selector).append(ul);
    } catch (err) {
      console.log(err);
    }
  });
}
authors(books, "#root");
