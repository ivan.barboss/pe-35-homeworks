const url = "https://ajax.test-danit.com/api/swapi/films";

axios
  .get(url)
  .then(({ status, data }) => {
    const container = document.querySelector(".container");
    if (status === 200) {
      data.forEach(({ episodeId, name, openingCrawl, characters, id }) => {
        container.insertAdjacentHTML(
          "beforeend",
          `<div class="card">
                     <p>Episode: ${episodeId}</p>
                     <p>Title: ${name}</p> 
                     <p>Characters:</p> 
                     <ul id = "characters-${id}"></ul>
                     <p>Summary: ${openingCrawl}</p>
                     </div>`
        );

        characters.forEach((element) => {
          axios
            .get(element)
            .then(({ status, data: { name } }) => {
              if (status === 200) {
                document
                  .querySelector(`#characters-${id}`)
                  .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
              }
            })
            .catch((error) => console.log(error));
        });
      });
    }
  })
  .catch((error) => console.log(error));
