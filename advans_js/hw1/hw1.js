"use strict";
class Employee {
  /**
   *
   * @param {string} name
   * @param {number} age
   * @param {number} salary
   */
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  /**
   *
   * @param {string} name
   * @param {number} age
   * @param {number} salary
   * @param {string} lang
   */
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

const prog = new Programmer("Ivan", 34, 1000, "english");
console.log(prog);
console.log(prog.name);
console.log(prog.age);
console.log(prog.salary);

const person = new Employee("Kat", 31, 3000);
console.log(person);
