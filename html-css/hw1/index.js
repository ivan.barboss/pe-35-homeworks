function createNewUser() {
  let firstName = prompt("firstName");
  let lastName = prompt("lastName");
  let birthday = prompt("dd.mm.yyyy");
  let newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLocaleLowerCase();
    },
    getAge: function () {
      return this.birthday;
    },
  };
  return newUser;
}
const user = createNewUser();
console.log(user.getLogin().getAge());
