import React from "react";
import { shallowEqual, useSelector } from "react-redux";
import styles from "./HeaderNav.module.scss";
import Icon from "@mdi/react";
import { Link } from "react-router-dom";
import { mdiCartMinus, mdiHome, mdiStarOutline } from "@mdi/js";

const HeaderNav = () => {
  const { data: cart } = useSelector((store) => store.cart, shallowEqual);
  const { data: cards } = useSelector((store) => store.card, shallowEqual);
  let favFlag = false;
  Object.values(cards).includes(true);
  const fav = cards.map((item) => item.isFavorite === true);
  favFlag = fav.includes(true);

  return (
    <nav>
      <ul>
        <li>
          <Link to="/" className={styles.navigationLink}>
            <Icon path={mdiHome} title="Home" size={1} />
            Home
          </Link>
        </li>
        <li>
          <Link to="/favorite" className={styles.navigationLink}>
            {!favFlag && <Icon path={mdiStarOutline} title="Empty" size={1} />}
            {favFlag && (
              <Icon
                path={mdiStarOutline}
                title="Buy it!"
                size={1}
                color="red"
              />
            )}
            Favorites
          </Link>
        </li>
        <li>
          <Link to="/cart" className={styles.navigationLink}>
            {cart.length < 1 && (
              <Icon path={mdiCartMinus} title="Empty" size={1} />
            )}
            {cart.length >= 1 && (
              <Icon path={mdiCartMinus} title="Buy it!" size={1} color="red" />
            )}
            Cart
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderNav;
