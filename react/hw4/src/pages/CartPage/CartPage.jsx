import React from "react";
import Section from "../../components/Section/Section";

const CartPage = () => {
  const localProps = {
    title: "CART",
  };

  return <Section title={localProps.title} />;
};

export default CartPage;
