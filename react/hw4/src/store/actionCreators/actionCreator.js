import { getAllCard, editCard } from "../../api";
import {
  INIT_CARDS,
  ADD_TO_CART,
  UPDATE_CART,
  DELETE_FROM_CART,
  INIT_FROM_CARTS,
  TOGGLE_IS_FAVORITE_CARDS,
  SET_IS_OPEN_MODAL,
  SET_CONFIG_MODAL,
} from "../actions/actions";

export const initCards = () => async (dispatch) => {
  await getAllCard().then(({ status, data }) => {
    if (status === 200) {
      dispatch({ type: INIT_CARDS, payload: data });
      return { status, data };
    } else {
      return { status, data };
    }
  });
};

export const toggleIsFavoriteCards = (cardID, body) => async (dispatch) => {
  body.isFavorite = !body.isFavorite;
  await editCard(cardID, body).then(({ status, data }) => {
    if (status === 200) {
      dispatch({ type: TOGGLE_IS_FAVORITE_CARDS, payload: data });
      return { status, data };
    } else {
      return { status, data };
    }
  });
};

export const initFromCarts = () => {
  return { type: INIT_FROM_CARTS, payload: "Cart" };
};
export const addToCart = (item) => ({ type: ADD_TO_CART, payload: item });

export const updateCart = (item) => ({ type: UPDATE_CART, payload: item });

export const deleteFromCart = (item) => ({
  type: DELETE_FROM_CART,
  payload: item,
});

export const setIsOpenModal = (value) => ({
  type: SET_IS_OPEN_MODAL,
  payload: value,
});
export const setConfigModal = (value) => ({
  type: SET_CONFIG_MODAL,
  payload: value,
});
