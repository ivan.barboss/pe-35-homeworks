import setCardsToLS from "../../functions/setCardsToLS";
import getCardsFromLS from "../../functions/getCardsFromLS";
import {
  ADD_TO_CART,
  UPDATE_CART,
  INIT_FROM_CARTS,
  DELETE_FROM_CART,
} from "../actions/actions";

const initialState = {
  data: [
    {
      id: null,
      name: "",
      price: null,
      image: "",
      vendorCode: null,
      color: "",
      isFavorite: false,
      quantity: 0,
    },
  ],
};

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case INIT_FROM_CARTS: {
      const newData = getCardsFromLS(payload);
      return { ...state, data: newData };
    }
    case ADD_TO_CART: {
      const newData = [...state.data];
      const targetIndex = newData.findIndex((e) => e.id === payload.id);
      if (targetIndex === -1) {
        payload.quantity = 1;
        newData.push(payload);
      } else {
        newData[targetIndex].quantity = newData[targetIndex].quantity + 1;
      }
      setCardsToLS("Cart", newData);
      return { data: newData };
    }

    case UPDATE_CART: {
      const newData = [...state.data];
      const targetIndex = newData.findIndex((e) => e.id === payload.id);
      if (targetIndex === -1) {
        console.log("UPDATE_CART find = -1");
      } else {
        newData[targetIndex].quantity = payload.quantity;
      }
      setCardsToLS("Cart", newData);
      return { data: newData };
    }

    case DELETE_FROM_CART: {
      const newData = [...state.data];
      const targetIndex = newData.findIndex((e) => e.id === payload.id);
      newData.splice(newData[targetIndex], 1);
      setCardsToLS("Cart", newData);
      return { data: newData };
    }
    default: {
      return state;
    }
  }
};

export default cartReducer;
