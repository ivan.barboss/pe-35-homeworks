import { SET_IS_OPEN_MODAL, SET_CONFIG_MODAL } from "../actions/actions";

const initialValues = {
  isOpen: false,
  id: "",
  title: "",
};

const modalReducer = (state = initialValues, action) => {
  switch (action.type) {
    case SET_IS_OPEN_MODAL: {
      return { ...state, isOpen: action.payload, id: action.payload?.id };
    }
    case SET_CONFIG_MODAL: {
      return { ...state, title: action.payload?.title, id: action.payload?.id };
    }

    default:
      return state;
  }
};

export default modalReducer;
