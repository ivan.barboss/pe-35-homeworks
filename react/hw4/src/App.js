import { Provider } from "react-redux";
import store from "./store";
import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes/Routes";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import { getTokenFromServer } from "../src/api/index";
import { setCookie } from "../src/utils/cookies";

function App() {
  const loginData = {
    email: "serhyi.minin@gmail.com",
    password: "K99d2VKzzpzAKqND",
  };

  useEffect(() => {
    getTokenFromServer(loginData).then(({ status, data }) => {
      if (status === 200) {
        setCookie("Token", data, 1);
        return { status, data };
      } else {
        return { status, data };
      }
    });
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <div className={styles.main}>
          <Header />
          <Routes />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
