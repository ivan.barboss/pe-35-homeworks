import PropTypes from "prop-types";
import React from "react";
import styles from "./Section.module.scss";
import CardList from "../CardList/CardList";
import SectionTitle from "../SectionTitle/SectionTitle";

const Section = (props) => {
  const { title, cards, addToCart, toggleFav, storedCards, deleteFromCart } =
    props;

  return (
    <section className={styles.container}>
      <SectionTitle title={title} />
      <div className={styles.cards}>
        <CardList
          title={title}
          addToCart={addToCart}
          toggleFav={toggleFav}
          cards={cards}
          storedCards={storedCards}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </section>
  );
};

Section.propTypes = {
  cards: PropTypes.array,
  storedCards: PropTypes.array,
  title: PropTypes.string.isRequired,
  addToCart: PropTypes.func,
  deleteFromCart: PropTypes.func,
  toggleFav: PropTypes.func.isRequired,
};

export default Section;
