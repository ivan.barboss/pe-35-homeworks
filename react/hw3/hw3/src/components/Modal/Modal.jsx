import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./Modal.module.scss";

function Modal(props) {
  const { closeModal, modalHeader, question, actions, className } = props;
  const handleClick = (e) => {
    if (e.target.className !== `${styles.main} ${className}`) {
      return;
    }
    closeModal();
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={`${styles.main} ${className}`}>
      <div className={styles.box}>
        <div className={`${styles.header} ${className}`}>
          <span className={styles.title}>{modalHeader}</span>
          <div className={styles.close} onClick={closeModal}></div>
        </div>
        <p className={styles.text}>{question}</p>
        <div className={styles.buttons}>{actions}</div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  actions: PropTypes.element,
  className: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  modalHeader: PropTypes.string,
  question: PropTypes.string,
};

export default Modal;
