import React from "react";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";
import HeaderNav from "../HeaderNav/HeaderNav";

const Header = (props) => {
  const { cards, storedCards } = props;

  return (
    <div className={styles.wrap}>
      <HeaderNav cards={cards} storedCards={storedCards} />
    </div>
  );
};

Header.propTypes = {
  cards: PropTypes.array.isRequired,
  storedCards: PropTypes.array.isRequired,
};

export default Header;
