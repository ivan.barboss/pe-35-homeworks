import React from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import buttonStyles from "../Button/Button.module.scss";

const CardList = (props) => {
  let { cards, addToCart, toggleFav, storedCards, title, deleteFromCart } =
    props;

  let arr = [];
  let btnText = "";
  let btnColor = null;
  let addOrDeleteInCart = null;
  let modalHeader = "BUY AN ITEM";
  let question = "Do you want to add the item to the shopping cart?";
  let btnModalTextConfirm = "Buy";

  switch (title) {
    case "CART":
      arr = storedCards;
      btnText = "DELETE FROM CART";
      btnColor = buttonStyles.red;
      toggleFav = () => {};
      addOrDeleteInCart = deleteFromCart;
      modalHeader = "DELETE FROM CART";
      question = "Are you serious?";
      btnModalTextConfirm = "Delete for now";
      break;
    case "LATEST ARRIVALS IN MUSICA":
      arr = cards;
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = addToCart;
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
    case "FAVORITE GOODS":
      arr = cards.filter((elem) => elem.isFavorite === true);
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = addToCart;
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
    default:
      arr = [];
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = addToCart;
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
  }

  return (
    arr &&
    arr.map((item, index) => {
      return (
        <Card
          {...item}
          toggleFav={toggleFav}
          key={item.name + index}
          addOrDeleteInCart={addOrDeleteInCart}
          btnText={btnText}
          btnColor={btnColor}
          modalHeader={modalHeader}
          question={question}
          btnModalTextConfirm={btnModalTextConfirm}
        />
      );
    })
  );
};

CardList.propTypes = {
  cards: PropTypes.array,
  addToCart: PropTypes.func,
  storedCards: PropTypes.array,
  title: PropTypes.string,
  deleteFromCart: PropTypes.func,
  toggleFav: PropTypes.func.isRequired,
};

export default CardList;
