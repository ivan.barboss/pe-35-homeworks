import React from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import styles from "./components/Button/Button.module.scss";
import Modal from "./components/Modal/Modal";
import colors from "./components/Modal/Modal.module.scss";

class App extends React.Component {
  state = {
    isOpenFirst: false,
    isCloseButtonFirst: false,
    isOpenSecond: false,
    isCloseButtonSecond: false,
  };

  openFirstModal = () => {
    this.setState((current) => ({
      ...current,
      isOpenFirst: true,
      isCloseButtonFirst: true,
    }));
  };

  openSecondModal = () => {
    this.setState((current) => ({
      ...current,
      isOpenSecond: true,
      isCloseButtonSecond: false,
    }));
  };

  closeModal = () => {
    this.setState((current) => ({
      ...current,
      isOpenFirst: false,
      isCloseButtonFirst: false,
      isOpenSecond: false,
      isCloseButtonSecond: false,
    }));
  };

  render() {
    return (
      <div className="App">
        <Button
          className={`${styles.button} ${styles.landing} ${styles.btn1}`}
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          className={`${styles.button} ${styles.landing} ${styles.btn2}`}
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.isOpenFirst && (
          <Modal
            closeModal={this.closeModal}
            className={`${colors.red}`}
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action.
            Are you sure you want to delete it?"
            isCloseButton={this.state.isCloseButtonFirst}
            actions={
              <>
                <Button
                  className={`${styles.button} ${styles.modal} ${styles.red}`}
                  text="Ok"
                />
                <Button
                  className={`${styles.button} ${styles.modal} ${styles.red}`}
                  text="Cancel"
                  onClick={this.closeModal}
                />
              </>
            }
          />
        )}

        {this.state.isOpenSecond && (
          <Modal
            className={`${colors.red_2}`}
            closeModal={this.closeModal}
            header="Do you want to move this file?"
            text="Once you move this file, it won’t be possible to undo this action.
            Are you sure you want to move it?"
            isCloseButton={this.state.isCloseButtonSecond}
            actions={
              <>
                <Button
                  className={`${styles.button} ${styles.modal} ${styles.red_2}`}
                  text="Ok"
                />
                <Button
                  className={`${styles.button} ${styles.modal} ${styles.red_2}`}
                  text="Cancel"
                  onClick={this.closeModal}
                />
              </>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
