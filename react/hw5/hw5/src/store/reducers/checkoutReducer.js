import { SET_IS_OPEN_CHECKOUT } from "../actions/actions";

const initialState = {
  isCheckout: false,
};

const checkoutReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_IS_OPEN_CHECKOUT:
      return { ...state, isCheckout: !state.isCheckout };
    default: {
      return state;
    }
  }
};

export default checkoutReducer;
