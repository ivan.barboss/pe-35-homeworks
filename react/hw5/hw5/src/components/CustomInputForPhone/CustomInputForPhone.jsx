import React, { memo } from "react";
import PropTypes from "prop-types";
import style from ".//CustomInputForPhone.module.scss";
import NumberFormat from "react-number-format";

const CustomInputForPhone = (props) => {
  const { label, field, form } = props;

  const touched = form.touched[field.name];
  const error = form.errors[field.name];

  return (
    <>
      <label>{label}</label>
      <NumberFormat
        format="+380 (##) ###-####"
        mask="_"
        allowEmptyFormatting={true}
        value={field.value}
        {...field}
      />
      <span className={style.error}>{touched && error ? error : ""}</span>
    </>
  );
};

CustomInputForPhone.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }),
  form: PropTypes.shape({
    errors: PropTypes.object,
    touched: PropTypes.object,
  }),
  label: PropTypes.string,
};
CustomInputForPhone.defaultProps = {
  label: "",
};

export default memo(CustomInputForPhone);
