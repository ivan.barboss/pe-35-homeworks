import React, { useEffect, memo } from "react";
import PropTypes from "prop-types";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import Card from "../Card/Card";
import buttonStyles from "../Button/Button.module.scss";
import {
  initCards,
  addToCart,
  deleteFromCart,
  initFromCarts,
} from "../../store/actionCreators/actionCreator";
// import axios from "axios"; //Temporary for pushing cards to server
// import { createCard } from "../../api"; //Temporary for pushing cards to server

const CardList = (props) => {
  const { data: cart } = useSelector((store) => store.cart, shallowEqual);
  let { title } = props;
  const { data: cards } = useSelector((store) => store.card, shallowEqual);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(initCards());
    dispatch(initFromCarts());
  }, []);

  //Temporary for pushing cards to server

  // useEffect(() => {
  //   const data = axios
  //     .get("http://localhost:3000/goods.json")
  //     .then((response) => {
  //       const { data } = response.data;

  //       data.forEach((item) => {
  //         createCard(item);
  //       });

  //       console.log("response.data: ", data);
  //       return response.data;
  //     });
  //   console.log("data: ", data);
  // }, []);

  let arr = [];
  let btnText = "";
  let btnColor = null;
  let addOrDeleteInCart = null;
  let modalHeader = "BUY AN ITEM";
  let question = "Do you want to add the item to the shopping cart?";
  let btnModalTextConfirm = "Buy";

  switch (title) {
    case "CART":
      arr = cart;
      btnText = "DELETE FROM CART";
      btnColor = buttonStyles.red;
      addOrDeleteInCart = (item) => dispatch(deleteFromCart(item));
      modalHeader = "DELETE FROM CART";
      question = "Are you serious?";
      btnModalTextConfirm = "Delete for now";
      break;
    case "LATEST ARRIVALS IN MUSICA":
      arr = cards;
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = (item) => dispatch(addToCart(item));
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
    case "FAVORITE GOODS":
      arr = cards.filter((elem) => elem.isFavorite === true);
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = (item) => dispatch(addToCart(item));
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
    default:
      arr = [];
      btnText = "ADD TO CART";
      btnColor = buttonStyles.black;
      addOrDeleteInCart = (item) => dispatch(addToCart(item));
      modalHeader = "BUY AN ITEM";
      question = "Do you want to add the item to the shopping cart?";
      btnModalTextConfirm = "Buy";
      break;
  }

  return (
    arr &&
    arr.map((item, index) => {
      return (
        <Card
          {...item}
          key={item.name + index}
          btnText={btnText}
          btnColor={btnColor}
          modalHeader={modalHeader}
          question={question}
          btnModalTextConfirm={btnModalTextConfirm}
          item={item}
          addOrDeleteInCart={addOrDeleteInCart}
        />
      );
    })
  );
};

CardList.propTypes = {
  title: PropTypes.string,
};

export default memo(CardList);
