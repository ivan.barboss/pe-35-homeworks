import React, { useState, memo } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import cardStyle from "./Card.module.scss";
import Button from "../Button/Button";
import buttonStyles from "../Button/Button.module.scss";
import Modal from "../Modal/Modal";
import colors from "../Modal/Modal.module.scss";
import Icon from "@mdi/react";
import { mdiStarOutline, mdiStar } from "@mdi/js";
import {
  toggleIsFavoriteCards,
  updateCart,
  setIsOpenModal,
} from "../../store/actionCreators/actionCreator";

const Card = (props) => {
  let {
    id,
    name,
    price,
    image,
    isFavorite,
    btnText,
    btnColor,
    addOrDeleteInCart,
    modalHeader,
    question,
    btnModalTextConfirm,
    item,
    quantity,
  } = props;

  const { isOpen, id: modalId } = useSelector(
    (state) => state.modal,
    shallowEqual
  );
  const [num, setNum] = useState(quantity);

  const closeModal = () => dispatch(setIsOpenModal({ isOpen: true, id: "" }));
  const dispatch = useDispatch();

  return (
    <div className={cardStyle.box} id={id}>
      <img src={image} alt={name} />
      <p>{name}</p>
      <div className={cardStyle.wrapper}>
        <div
          className={cardStyle.favorites}
          onClick={() => dispatch(toggleIsFavoriteCards(id, item))}>
          {!isFavorite && (
            <Icon path={mdiStarOutline} title="not Favorite" size={1} />
          )}
          {isFavorite && (
            <Icon path={mdiStar} title="Favorite" size={1} color="red" />
          )}
        </div>
        {quantity && (
          <>
            <span>Quantity:</span>

            <input
              className={cardStyle.quantity}
              type="number"
              min={1}
              max={10000}
              value={num}
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onChange={(e) => {
                setNum(e.target.value);
                item = { ...item, quantity: e.target.value };
                dispatch(updateCart(item));
              }}
            />
          </>
        )}
      </div>
      <div className={cardStyle.line}>
        <span className={cardStyle.price}>${price}</span>
        <Button
          className={`${buttonStyles.button} ${buttonStyles.landing} ${btnColor}`}
          btnText={btnText}
          onClick={() => {
            dispatch(setIsOpenModal({ isOpen: true, id: id }));
          }}
        />
      </div>

      {isOpen && id === modalId && (
        <Modal
          className={`${colors.violet}`}
          modalHeader={modalHeader}
          question={question}
          actions={
            <>
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText={btnModalTextConfirm}
                onClick={() => {
                  addOrDeleteInCart(item);
                  closeModal();
                }}
              />
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText="Cancel"
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

Card.propTypes = {
  addOrDeleteInCart: PropTypes.func.isRequired,
  btnColor: PropTypes.string,
  btnModalTextConfirm: PropTypes.string.isRequired,
  btnText: PropTypes.string.isRequired,
  id: PropTypes.number,
  image: PropTypes.string,
  isFavorite: PropTypes.bool.isRequired,
  item: PropTypes.object.isRequired,
  modalHeader: PropTypes.string.isRequired,
  name: PropTypes.string,
  price: PropTypes.number,
  quantity: PropTypes.number, //revision expected
  question: PropTypes.string.isRequired,
};

export default memo(Card);
