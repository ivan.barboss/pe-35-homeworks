import React, { memo } from "react";
import styles from "./ButtonOrder.module.scss";
import Button from "@mui/material/Button";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setIsOpenCheckout } from "../../store/actionCreators/actionCreator";

const ButtonOrder = () => {
  const dispatch = useDispatch();
  const { data: cart } = useSelector((store) => store.cart, shallowEqual);

  return (
    <section className={styles.container}>
      {cart.length > 0 ? (
        <Button
          variant="contained"
          className={styles.Button}
          color="error"
          onClick={() => dispatch(setIsOpenCheckout(true))}>
          checkout
        </Button>
      ) : (
        <Button
          disabled
          variant="contained"
          className={styles.Button}
          color="error"
          onClick={() => dispatch(setIsOpenCheckout(true))}>
          checkout
        </Button>
      )}
    </section>
  );
};

export default memo(ButtonOrder);
